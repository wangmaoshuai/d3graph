/**
 * Created by wangmsh on 2017/5/3.
 */
const $ = require('jquery');
const d3 = require('d3');

$(document).ready(() => {
    const width = 800;
    const height = 800;
    // force layout setup
    const force = d3.layout.force()
        .charge(-200).linkDistance(30).size([width, height]);

    // setup svg div
    const svg = d3.select('#graph').append('svg')
        .attr('width', '800px').attr('height', '800px')
        .attr('pointer-events', 'all');

    // load graph (nodes,links) json from /graph endpoint
    d3.json('../data/neo4j.json', (error, graph) => {
        if (error) return;

        force.nodes(graph.nodes).links(graph.links).start();

        // render relationships as lines
        const link = svg.selectAll('.link')
            .data(graph.links).enter()
            .append('line')
            .attr('class', 'link');

        // render nodes as circles, css-class from label
        const node = svg.selectAll('.node')
            .data(graph.nodes).enter()
            .append('circle')
            .attr('class', d => `node ${d.label}`)
            .attr('r', 10)
            .call(force.drag);

        // html title attribute for title node-attribute
        node.append('title')
            .text(d => d.title);

        // force feed algo ticks for coordinate computation
        force.on('tick', () => {
            link.attr('x1', d => d.source.x)
                .attr('y1', d => d.source.y)
                .attr('x2', d => d.target.x)
                .attr('y2', d => d.target.y);

            node.attr('cx', d => d.x)
                .attr('cy', d => d.y);
        });
    });
});
