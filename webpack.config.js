/**
 * Created by Diablo on 2017/4/20.
 * desc:webpack 的配置文件
 */

const path = require('path');
const webpack = require('webpack');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
    context: path.resolve(__dirname, './js'),
    entry: {
        d3Graph: ['./d3Graph.js'],
        d3GraphMenu: ['./d3Graph-menu.js'],
        d3GraphMenuEs6: ['./d3Graph-menu-es6'],
        vendor: ['lodash', 'jquery', 'd3'],
    },
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: '[name].bundle.js',
    },
    devServer: {
        contentBase: path.join(__dirname, './'),
        compress: false,
        port: 9000,
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env'],
                    },
                },
            },
        ],
    },
    devtool: 'source-map',
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({
            names: ['common', 'vendor'],
            minChunks(module) {
                return module.context && module.context.indexOf('node_modules') !== -1;
            },
        }),
        new UglifyJSPlugin({
            sourceMap: true,
        }),
    ],
};
