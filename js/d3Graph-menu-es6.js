/**
 * Created by Diablo on 2017/5/4.
 */
import d3 from 'd3';
import $ from 'jquery';
import RenderGraph from './RenderGraph';

const json = {
    nodes: [
        {
            id: 3,
            name: 'test3',
            group: 1,
        },
        {
            id: 4,
            name: 'tes4',
            group: 1,
        },
    ],
    links: [{
        source: 3,
        target: 4,
        value: 1,
        id: 2,
    }],
};
$(document).ready(() => {
    const width = 1000;
    const height = 1000;
    const svg = d3.select('#graph').append('svg')
        .attr('width', width)
        .attr('height', height)
        .attr('pointer-events', 'all');
// Per-type markers, as they don't inherit styles.
    svg.append('svg:defs').selectAll('marker')
        .data(['a'])
        .enter()
        .append('marker')
        .attr('id', d => d)
        .attr('viewBox', '0 -5 10 10')
        .attr('refX', 15)
        .attr('refY', -1.5)
        .attr('markerWidth', 6)
        .attr('markerHeight', 6)
        .attr('orient', 'auto')
        .append('path')
        .attr('d', 'M0,-5L10,0L0,5');

// build the arrow.
    svg.append('svg:defs').selectAll('marker')
        .data(['end'])      // Different link/path types can be defined here
        .enter()
        .append('svg:marker')    // This section adds in the arrows
        .attr('id', String)
        .attr('viewBox', '0 -5 10 10')
        .attr('refX', 18)
        .attr('refY', -1.08)
        .attr('markerWidth', 8)
        .attr('markerHeight', 8)
        .attr('orient', 'auto')
        .append('svg:path')
        .attr('d', 'M0,-5L10,0L0,5');

    const graph = new RenderGraph(width, height, svg);
    graph.update(json);
});
