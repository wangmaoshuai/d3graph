/**
 * Created by wangmsh on 2017/5/3.
 */
const d3 = require('d3');
const $ = require('jquery');

const json = {
    nodes: [{
        id: -1146034065,
        name: '/',
        group: 0,
    }, {
        id: -990073683,
        name: '/blog/',
        group: 0,
    }, {
        id: -1724280020,
        name: '/menu/',
        group: 0,
    }, {
        id: 1176095248,
        name: '/napojovy-listek/',
        group: 0,
    }, {
        id: -2085082741,
        name: '/fotogalerie/',
        group: 0,
    }, {
        id: 883542796,
        name: '/rezervace/',
        group: 0,
    }, {
        id: 369131020,
        name: '/kontakt/',
        group: 0,
    }, {
        id: -1276353015,
        name: '/en/',
        group: 0,
    }, {
        id: -1557747058,
        name: '/o-nas/',
        group: 404,
    }, {
        id: 890427810,
        name: '/en/about-us/',
        group: 0,
    }, {
        id: -978700858,
        name: '/en/menu-2/',
        group: 0,
    }, {
        id: 1436673749,
        name: '/en/napojovy-listek/',
        group: 0,
    }, {
        id: -489730654,
        name: '/en/photograph/',
        group: 0,
    }, {
        id: -1461616187,
        name: '/en/reservation/',
        group: 0,
    }, {
        id: 1520755615,
        name: '/en/contact/',
        group: 0,
    }, {
        id: 37644686,
        name: '/en//kontakt/',
        group: 0,
    }, {
        id: 1131720527,
        name: '/en//o-nas/',
        group: 404,
    }],
    links: [{
        source: -990073683,
        target: -1146034065,
        value: 1,
    }, {
        source: -1724280020,
        target: -1146034065,
        value: 1,
    }, {
        source: 1176095248,
        target: -1146034065,
        value: 1,
    }, {
        source: -2085082741,
        target: -1146034065,
        value: 1,
    }, {
        source: 883542796,
        target: -1146034065,
        value: 1,
    }, {
        source: 369131020,
        target: -1146034065,
        value: 1,
    }, {
        source: -1276353015,
        target: -1146034065,
        value: 1,
    }, {
        source: -1557747058,
        target: -1146034065,
        value: 1,
    }, {
        source: 890427810,
        target: -990073683,
        value: 1,
    }, {
        source: -978700858,
        target: -1724280020,
        value: 1,
    }, {
        source: 1436673749,
        target: 1176095248,
        value: 1,
    }, {
        source: -489730654,
        target: -2085082741,
        value: 1,
    }, {
        source: -1461616187,
        target: 883542796,
        value: 1,
    }, {
        source: 1520755615,
        target: 369131020,
        value: 1,
    }, {
        source: 37644686,
        target: -1276353015,
        value: 1,
    }, {
        source: 1131720527,
        target: -1276353015,
        value: 1,
    }, {
        source: -1146034065,
        target: -1146034065,
        count: 1,
        value: 1,
    }, {
        source: -1146034065,
        target: -990073683,
        count: 1,
        value: 1,
    }, {
        source: -1146034065,
        target: -1724280020,
        count: 3,
        value: 1,
    }, {
        source: -1146034065,
        target: 1176095248,
        count: 3,
        value: 1,
    }, {
        source: -1146034065,
        target: -2085082741,
        count: 3,
        value: 1,
    }, {
        source: -1146034065,
        target: 883542796,
        count: 3,
        value: 1,
    }, {
        source: -1146034065,
        target: 369131020,
        count: 3,
        value: 1,
    }, {
        source: -1146034065,
        target: -1276353015,
        count: 1,
        value: 1,
    }, {
        source: -1146034065,
        target: -1557747058,
        count: 2,
        value: 1,
    }, {
        source: -990073683,
        target: -1146034065,
        count: 1,
        value: 1,
    }, {
        source: -990073683,
        target: -990073683,
        count: 1,
        value: 1,
    }, {
        source: -990073683,
        target: -1724280020,
        count: 1,
        value: 1,
    }, {
        source: -990073683,
        target: 1176095248,
        count: 1,
        value: 1,
    }, {
        source: -990073683,
        target: -2085082741,
        count: 1,
        value: 1,
    }, {
        source: -990073683,
        target: 883542796,
        count: 1,
        value: 1,
    }, {
        source: -990073683,
        target: 369131020,
        count: 1,
        value: 1,
    }, {
        source: -990073683,
        target: 890427810,
        count: 1,
        value: 1,
    }, {
        source: -1724280020,
        target: -1146034065,
        count: 1,
        value: 1,
    }, {
        source: -1724280020,
        target: -990073683,
        count: 1,
        value: 1,
    }, {
        source: -1724280020,
        target: -1724280020,
        count: 1,
        value: 1,
    }, {
        source: -1724280020,
        target: 1176095248,
        count: 1,
        value: 1,
    }, {
        source: -1724280020,
        target: -2085082741,
        count: 1,
        value: 1,
    }, {
        source: -1724280020,
        target: 883542796,
        count: 1,
        value: 1,
    }, {
        source: -1724280020,
        target: 369131020,
        count: 1,
        value: 1,
    }, {
        source: -1724280020,
        target: -978700858,
        count: 1,
        value: 1,
    }, {
        source: 1176095248,
        target: -1146034065,
        count: 1,
        value: 1,
    }, {
        source: 1176095248,
        target: -990073683,
        count: 1,
        value: 1,
    }, {
        source: 1176095248,
        target: -1724280020,
        count: 1,
        value: 1,
    }, {
        source: 1176095248,
        target: 1176095248,
        count: 1,
        value: 1,
    }, {
        source: 1176095248,
        target: -2085082741,
        count: 1,
        value: 1,
    }, {
        source: 1176095248,
        target: 883542796,
        count: 1,
        value: 1,
    }, {
        source: 1176095248,
        target: 369131020,
        count: 1,
        value: 1,
    }, {
        source: 1176095248,
        target: 1436673749,
        count: 1,
        value: 1,
    }, {
        source: -2085082741,
        target: -1146034065,
        count: 1,
        value: 1,
    }, {
        source: -2085082741,
        target: -990073683,
        count: 1,
        value: 1,
    }, {
        source: -2085082741,
        target: -1724280020,
        count: 1,
        value: 1,
    }, {
        source: -2085082741,
        target: 1176095248,
        count: 1,
        value: 1,
    }, {
        source: -2085082741,
        target: -2085082741,
        count: 1,
        value: 1,
    }, {
        source: -2085082741,
        target: 883542796,
        count: 1,
        value: 1,
    }, {
        source: -2085082741,
        target: 369131020,
        count: 1,
        value: 1,
    }, {
        source: -2085082741,
        target: -489730654,
        count: 1,
        value: 1,
    }, {
        source: 883542796,
        target: -1146034065,
        count: 1,
        value: 1,
    }, {
        source: 883542796,
        target: -990073683,
        count: 1,
        value: 1,
    }, {
        source: 883542796,
        target: -1724280020,
        count: 1,
        value: 1,
    }, {
        source: 883542796,
        target: 1176095248,
        count: 1,
        value: 1,
    }, {
        source: 883542796,
        target: -2085082741,
        count: 1,
        value: 1,
    }, {
        source: 883542796,
        target: 883542796,
        count: 1,
        value: 1,
    }, {
        source: 883542796,
        target: 369131020,
        count: 1,
        value: 1,
    }, {
        source: 883542796,
        target: -1461616187,
        count: 1,
        value: 1,
    }, {
        source: 369131020,
        target: -1146034065,
        count: 1,
        value: 1,
    }, {
        source: 369131020,
        target: -990073683,
        count: 1,
        value: 1,
    }, {
        source: 369131020,
        target: -1724280020,
        count: 1,
        value: 1,
    }, {
        source: 369131020,
        target: 1176095248,
        count: 1,
        value: 1,
    }, {
        source: 369131020,
        target: -2085082741,
        count: 1,
        value: 1,
    },

    ],
};
const width = 1000;
const height = 1000;

$(document).ready(() => {
    const svg = d3.select('#graph').append('svg')
        .attr('width', width)
        .attr('height', height)
        .attr('pointer-events', 'all');
// Per-type markers, as they don't inherit styles.
    svg.append('defs').selectAll('marker')
        .data(['a'])
        .enter()
        .append('marker')
        .attr('id', d => d)
        .attr('viewBox', '0 -5 10 10')
        .attr('refX', 15)
        .attr('refY', -1.5)
        .attr('markerWidth', 6)
        .attr('markerHeight', 6)
        .attr('orient', 'auto')
        .append('path')
        .attr('d', 'M0,-5L10,0L0,5');

    const edges = [];
    const fill = d3.scale.category10();

    json.links.forEach((e) => {
        // Get the source and target nodes
        const sourceNode = json.nodes.filter(n => n.id === e.source)[0];
        const targetNode = json.nodes.filter(n => n.id === e.target)[0];
        const count = e.count;

        // Add the edge to the array
        edges.push({
            source: sourceNode,
            target: targetNode,
            count,
            type: 'a',
        });
    });

    const force = d3.layout.force()
        .charge(-200)
        .linkDistance(150)
        .size([width, height])
        .nodes(json.nodes)
        .links(edges)
        .start();


    const link = svg.append('g').selectAll('link')
        .data(edges)
        .enter()
        .append('path')
        .attr('class', 'link')
        .attr('marker-end', d => `url(#${d.targetNode})`)
        .style('stroke-width', d => Math.sqrt(d.count * 1, 5));


    const node = svg.selectAll('node')
        .data(json.nodes)
        .enter().append('g')
        .attr('class', 'node')
        .style('fill', d => fill(d.group))
        .call(force.drag)
        .on('mouseover', fade(0.1))
        .on('mouseout', fade(1))
        .on('contextmenu', function (d, i) {
            $(document).bind('contextmenu', () => false);
            svg.selectAll('.node').style('fill', d => fill(d.group));
            d3.select('.menu').remove();

            const thisNode = d3.select(this);
            thisNode.attr('r', 25).style('fill', 'lightcoral');

            const menuDataSet = [{
                size: 2,
                label: '添加节点',
            }, {
                size: 1,
                label: 'Item 2',
            }, {
                size: 65,
                label: 'Item 3',
            }, {
                size: 45,
                label: 'Item 4',
            }, {
                size: 50,
                label: 'Item 5',
            }];

            // Barvy menu
            const color = d3.scale.category20();
            const pie = d3.layout.pie()
                .sort(null)
                .value(d => Object.keys(menuDataSet).length);

            // Menu
            const widthMenu = 180;
            const heightMenu = 180;
            const radiusMenu = Math.min(widthMenu, heightMenu) / 2;

            // Arc setting
            const arc = d3.svg.arc()
                .innerRadius(radiusMenu - 70)
                .outerRadius(radiusMenu - 25);

            // Graph space
            const svgMenu = thisNode.append('svg')
                .attr('width', widthMenu)
                .attr('height', heightMenu)
                .attr('class', 'menu')
                .attr('x', -90)
                .attr('y', -90)
                .append('g')
                .attr('transform', `translate(${widthMenu / 2},${heightMenu / 2})`);


            // Prepare graph and load data
            const g = svgMenu.selectAll('.arc')
                .data(pie(menuDataSet))
                .enter().append('g')
                .attr('class', 'arc');

            // Add colors
            const path = g.append('path')
                .attr('d', arc)
                .attr('fill', d => color(d.data.size));

            // Add labels
            const asdfd = g.append('text')
                .attr('transform', d => `translate(${arc.centroid(d)})`)
                .attr('dy', '.35em')
                .style('text-anchor', 'middle')
                .text(d => d.data.label);

            // Add hover action
            path.on('mouseenter', function (d, i) {
                const thisPath = d3.select(this);
                thisPath.attr('fill', 'blue')
                    .attr('cursor', 'pointer')
                    .attr('class', 'on');
            });

            path.on('mouseout', function (d) {
                d3.select(this)
                    .attr('fill', d => color(d.data.size))
                    .attr('class', 'off');
            });
        });


    // node.append('image')
    //     .attr('xlink:href', 'https://github.com/favicon.ico')
    //     .attr('x', -8)
    //     .attr('y', -8)
    //     .attr('width', 16)
    //     .attr('height', 16);

    node.append('circle').attr('r', 10);

    node.append('text')
        .attr('dx', 12)
        .attr('dy', '.35em')
        .text(d => d.name);

    const labels = svg.selectAll('text')
        .data(edges)
        .enter().append('text')
        .attr('x', d => (d.source.y + d.target.y) / 2)
        .attr('y', d => (d.source.x + d.target.x) / 2)
        .attr('text-anchor', 'middle')
        .text(d => d.count);


    force.on('tick', () => {
        link.attr('d', (d) => {
            const dx = d.target.x - d.source.x;
            const dy = d.target.y - d.source.y;
            const dr = Math.sqrt(dx * dx + dy * dy);
            return `M${d.source.x},${d.source.y} L${d.target.x},${d.target.y}`;
        },
        );

        node.attr('transform', d => `translate(${d.x},${d.y})`);

        labels.attr('x', d => (d.source.x + d.target.x + 10) / 2)
            .attr('y', d => (d.source.y + d.target.y + 10) / 2);
    });


    function transform(d) {
        return `translate(${d.x},${d.y})`;
    }

    const linkedByIndex = {};
    edges.forEach((d) => {
        linkedByIndex[`${d.source.index},${d.target.index}`] = 1;
    });

    function isConnected(a, b) {
        return linkedByIndex[`${a.index},${b.index}`] || linkedByIndex[`${b.index},${a.index}`] || a.index == b.index;
    }


    function fade(opacity) {
        return function (d) {
            const labels = svg.selectAll('text')
                .data(edges)
                .enter().append('text')
                .attr('x', o => (o.source.y + o.target.y) / 2)
                .attr('y', o => (o.source.x + o.target.x) / 2)
                .attr('text-anchor', 'middle')
                .text(o => o.count);

            node.style('stroke-opacity', function (o) {
                const thisOpacity = isConnected(d, o) ? 1 : opacity;
                this.setAttribute('fill-opacity', thisOpacity);
                return thisOpacity;
            });

            link.style('stroke-opacity', o => o.source === d || o.target === d ? 1 : opacity);
        };
    }
});

