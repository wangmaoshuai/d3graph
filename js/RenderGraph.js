/**
 * Created by Diablo on 2017/5/4.
 */
import d3 from 'd3';
import $ from 'jquery';

export default class RenderGraph {
    constructor(width, height, svg) {
        this.width = width;
        this.height = height;
        this.svg = svg;
    }

    update(json) {
        const renderGraph = this;
        const fill = d3.scale.category10();
        const linkedByIndex = {};
        const edges = [];

        function isConnected(a, b) {
            return linkedByIndex[`${a.index},${b.index}`] || linkedByIndex[`${b.index},${a.index}`] || a.index === b.index;
        }

        function transform(d) {
            return `translate(${d.x},${d.y})`;
        }

        json.links.forEach((e) => {
            // Get the source and target nodes
            const sourceNode = json.nodes.filter(n => n.id === e.source)[0];
            const targetNode = json.nodes.filter(n => n.id === e.target)[0];
            const count = e.count;
            // Add the edge to the array
            edges.push({
                source: sourceNode,
                target: targetNode,
                count,
                type: 'a',
            });
        });
        const force = d3.layout.force()
            .charge(-200)
            .linkDistance(150)
            .size([renderGraph.width, renderGraph.height])
            .nodes(json.nodes)
            .links(edges)
            .start();

        const link = renderGraph.svg.append('g').selectAll('link')
            .data(edges);

        link.enter()
            .append('path')
            .attr('id', d => `${d.source.id}-${d.target.id}`)
            .attr('class', 'link')
            .attr('marker-end', 'url(#end)');
        // .style('stroke-width', d => Math.sqrt(d.count * 1, 5));


        const node = renderGraph.svg.selectAll('node')
            .data(json.nodes, d => d.id);

        renderGraph.svg.on('click', () => {
            d3.select('.menu').remove();
        });

        node.enter().append('g')
            .attr('class', 'node')
            .style('fill', d => fill(d.group))
            .call(force.drag)
            .on('mouseover', fade(0.1))
            .on('mouseout', fade(1))
            .on('contextmenu', function (d, i) {
                $(document).bind('contextmenu', () => false);
                renderGraph.svg.selectAll('.node').style('fill', d => fill(d.group));
                d3.select('.menu').remove();

                const thisNode = d3.select(this);
                thisNode.attr('r', 25).style('fill', 'lightcoral');

                const menuDataSet = [{
                    size: 2,
                    label: '添加节点',
                }, {
                    size: 1,
                    label: '收回节点',
                }, {
                    size: 65,
                    label: 'Item 3',
                }, {
                    size: 45,
                    label: '删除节点',
                }, {
                    size: 50,
                    label: 'Item 5',
                }];

                // Barvy menu
                const color = d3.scale.category20();
                const pie = d3.layout.pie()
                    .sort(null)
                    .value(d => Object.keys(menuDataSet).length);

                // Menu
                const widthMenu = 180;
                const heightMenu = 180;
                const radiusMenu = Math.min(widthMenu, heightMenu) / 2;

                // Arc setting
                const arc = d3.svg.arc()
                    .innerRadius(radiusMenu - 70)
                    .outerRadius(radiusMenu - 25);

                // Graph space
                const svgMenu = thisNode.append('svg')
                    .attr('width', widthMenu)
                    .attr('height', heightMenu)
                    .attr('class', 'menu')
                    .attr('x', -90)
                    .attr('y', -90)
                    .append('g')
                    .attr('transform', `translate(${widthMenu / 2},${heightMenu / 2})`);


                // Prepare graph and load data
                const g = svgMenu.selectAll('.arc')
                    .data(pie(menuDataSet))
                    .enter().append('g')
                    .attr('class', 'arc');

                // Add colors
                const path = g.append('path')
                    .attr('d', arc)
                    .attr('fill', d => color(d.data.size));

                // Add labels
                const asdfd = g.append('text')
                    .attr('transform', d => `translate(${arc.centroid(d)})`)
                    .attr('dy', '.35em')
                    .style('text-anchor', 'middle')
                    .text(d => d.data.label);

                // Add hover action
                path.on('mouseenter', function (d, i) {
                    const thisPath = d3.select(this);
                    thisPath.attr('fill', 'blue')
                        .attr('cursor', 'pointer')
                        .attr('class', 'on');
                });
                path.on('click', (d) => {
                    if (d.data.label === '删除节点') {
                        thisNode.remove();
                        force.links().forEach((link) => {
                            const nodeId = thisNode.select('circle').attr('id');
                            if (link.target.id === Number(nodeId)
                                || link.source.id === Number(nodeId)) {
                                renderGraph.svg.selectAll('path').attr('id', `#${link.source.id}-${link.target.id}`).remove();
                            }
                        });
                    } else if (d.data.label === '添加节点') {
                        d3.json('../data/update.json', update => renderGraph.update(update));
                    } else if (d.data.label === '收回节点') {
                        console.log(thisNode.child());
                    }
                });
                path.on('mouseout', function (d) {
                    d3.select(this)
                        .attr('fill', d => color(d.data.size))
                        .attr('class', 'off');
                });
            })
            .on('click', (d, i) => {
                d3.select('.menu').remove();
            });
        node.append('circle').attr('r', 10).attr('id', d => d.id);
        node.append('text')
            .attr('dx', 12)
            .attr('dy', '.35em')
            .text(d => d.name);

        const labels = renderGraph.svg.selectAll('text')
            .data(edges)
            .enter().append('text')
            .attr('x', d => (d.source.y + d.target.y) / 2)
            .attr('y', d => (d.source.x + d.target.x) / 2)
            .attr('text-anchor', 'middle')
            .text(d => d.count);


        force.on('tick', () => {
            link.attr('d', d => `M${d.source.x},${d.source.y} L${d.target.x},${d.target.y}`,
            );

            node.attr('transform', d => `translate(${d.x},${d.y})`);

            labels.attr('x', d => (d.source.x + d.target.x + 10) / 2)
                .attr('y', d => (d.source.y + d.target.y + 10) / 2);
        });
        edges.forEach((d) => {
            linkedByIndex[`${d.source.index},${d.target.index}`] = 1;
        });

        function fade(opacity) {
            return function (d) {
                const labels = renderGraph.svg.selectAll('text')
                    .data(edges)
                    .enter().append('text')
                    .attr('x', o => (o.source.y + o.target.y) / 2)
                    .attr('y', o => (o.source.x + o.target.x) / 2)
                    .attr('text-anchor', 'middle')
                    .text(o => o.count);

                node.style('stroke-opacity', function (o) {
                    const thisOpacity = isConnected(d, o) ? 1 : opacity;
                    this.setAttribute('fill-opacity', thisOpacity);
                    return thisOpacity;
                });

                link.style('stroke-opacity', o => o.source === d || o.target === d ? 1 : opacity);
            };
        }
    }
}
